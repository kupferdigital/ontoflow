# OntoFlow

OntoFlow is an **ontology development** workflow, build with Docker and [nextflow](https://www.nextflow.io/). Create your ontology **collaboratively** with [Chowlk](https://chowlk.linkeddata.es/chowlk_spec). OntoFlow **validates** your ontology against SHACL shapes, **documents** it with [pyLODE](https://github.com/RDFLib/pyLODE) and **publishes** it on GitLab Pages.

The copper [ontology](https://kupferdigital.gitlab.io/ontologies/copper) was developed with OntoFlow.

<img src="./ressources/demo.gif" height="600">

## Features

- Define your ontology either as text or graphically with Chowlk.
- Work with your domain expert at the same time on the same ontology diagram.
- Validate your ontology against provided SHACL shapes, which will ensure it conforms to best practices.
- Provide your own SHACL shapes to validate against.
- No installation needed, just add a CI job to your GitLab repository hosting your ontology.
- Easily modifiable to your needs, OntoFlow is a bunch of highly useful bash scripts wired together with nextflow.

## Installation

### `Local installation`

1. Install [Docker](https://docs.docker.com/engine/install/)

1. Install Java:

   ```bash
   sudo apt install openjdk-11-jre-headless
   ```

1. Install und run Nextflow:

   ```bash
   mkdir ~/.local/bin/
   cd ~/.local/bin/
   curl -s https://get.nextflow.io | bash
   ```

### `As a Gitlab CI Job`

1. Make sure your ontology source files are located at the root of your repository.
1. Add [.gitlab-ci.yml](https://gitlab.com/infai/ontoflow/-/blob/main/ressources) to your repository.

   ```bash
   # For docker runner, take this when in doubt
   curl https://gitlab.com/infai/ontoflow/-/raw/main/ressources/docker/.gitlab-ci.yml --output .gitlab-ci.yml
   # For podman runner
   curl https://gitlab.com/infai/ontoflow/-/raw/main/ressources/podman/.gitlab-ci.yml --output .gitlab-ci.yml
   ```

1. Commit and push your change.

   ```bash
   git add .
   git commit -m "Add OntoFlow CI job"
   git push
   ```

### `Display the pipeline status as a badge`

The latest information about badges can be retrieved from the [official documentation](https://docs.gitlab.com/ee/user/project/badges.html), but in short:

- Go to your project settings and expand the `Badges` section
- `Name` should be something meaningful like `OntoFlow`
- `Link` should point to the GitLab-Page of your group, in our case it would be `https://infai.gitlab.io/ontoflow/` 
- Enter the following URL under `Badge Image URL`:  https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg?style=flat-square&ignore_skipped=true&key_text=OntoFlow&key_width=80
 - Click on `Add badge` to get a nice badge at the top of your repsoitory's main page that informs you of OntoFlow's status

## Usage

- For test purposes you can download the input data from the repository or follow the tutorial on the [Chowlk-website](https://chowlk.linkeddata.es/)
- The [overview](#overview) also provides instructions for editing ontologies

### `Running locally`

- All RDF files and Chowlk diagrams should be in a directory and you can run OntoFlow locally by:

  ```bash
  cd PATH-OF-YOUR-ONTOLGY
  nextflow pull https://gitlab.com/infai/ontoflow -r main
  nextflow run https://gitlab.com/infai/ontoflow -r main
  ```

  > First run takes some time, because all the images have to be pulled.

- After this, documentation and validation results will appear in the `public/` folder and contains:

  | `index.html` | `index.jsonld` | `index.ttl` | `<ontology>.png` | `validation-report.ttl` |

### `Running as a CI Job`

- All RDF Files und Chowlk diagrams should be also in the repository.

- Add the [.gitlab-ci.yml](https://gitlab.com/infai/ontoflow/-/blob/main/ressources/.gitlab-ci.yml) to your repository and push it.

- After the push is done, the pipeline starts automatically in the repository (the process is shown in the Git GUI).

- The artifacts of the CI Job contains the output files and is downloadable.

  - To download the artifacts, you need to open your repository in the Git GUI.
  - There, you go to `CI/CD` &rightarrow; `Pipelines` &rightarrow; `<Pipeline ID>` &rightarrow; `Jobs` &rightarrow; `Download artifacts`_:Downloadbutton_

- The ontology documentation will be accessible through the URL: `https://GROUP_NAME.gitlab.io/PROJECT_NAME`.

## Overview

OntoFlow aligns ontology development, with how computer programs are written today:

1. Collaboratively edit source code in a git repository.
1. A continuous integration process takes those source files and:
   1. Builds an artifact.
   1. Tests the artifact.
   1. Deploys the artifact.

For ontology development usage of git often is difficult, because the ontology is designed by a domain expert, which often do not have a programming background and thus no git experience.

OntoFlow works around this issue with Chowlk.
Chowlk is a visual editor for ontologies, implemented as a library of elements for the diagramming software [diagrams.net](https://www.diagrams.net/).
You can use a GitLab repository as the storage backend for diagrams.net.
So you can have your cake (your ontology as source code in a git repository) and eat it too (your domain expert can drag and drop the ontology together with a GUI).
Thanks to diagrams.net a synchronize function even lets multiple persons work at the same diagram at the same time.

OntoFlow does not reinvent the wheel and tries to use already existing tools for developing and processing ontologies.
This has one drawback, those tools are distributed in many different ways.
It would take for the user some time to get all dependencies up and running.
So we package all tools as container images. Pulling these images, starting the containers and mounting the workflow files is handled by nextflow.

<img src="./ressources/flowchart.png" height="600">

### Edit Ontology with Chowlk

1. Navigate to [diagrams.net](https://www.diagrams.net/) and press `Start`.
1. Select `GitLab` as storage backend.
1. Create new diagram in the root of your repository.
1. Navigate to `File` &rightarrow; `Open Library from` &rightarrow; `URL...` and insert: `https://chowlk.linkeddata.es/resources/chowlk-drawio-library.xml`
1. Navigate to `File` &rightarrow; `Properties...` and uncheck `Compression`

> Chowlk also provides [documentation](https://chowlk.linkeddata.es/chowlk_spec)

### Edit Ontology without Chowlk

It is not possible to create a generic RDF graph with Chowlk.
If you have triples you can not, or do not want to express with a Chowlk diagram you can also use a serialisation of RDF as input for OntoFlow.
It is also possible to split the ontology on multiple files and mix those with Chowlk diagrams, just put all file in the root of your repository.

### Collaboratively edit with Chowlk

Multiple editors can open the same file and synchronize their changes by clicking `File` &rightarrow; `Synchronize`.
Here is an [article](https://drawio.freshdesk.com/support/solutions/articles/16000087947-synchronize-and-merge-external-changes-to-your-diagram) which goes into further detail.

### Validating

OntoFlow provides [SHACL shapes](https://gitlab.com/infai/ontoflow/-/tree/main/shacl) to validate your ontology against.
Currently, we use a subset of the [SHARK SHACL-Shapes for ontologies](https://github.com/gcpdev/shark/blob/master/web/guideline_form.json) in order to check whether the tested ontology complies to common rules and best practices for ontology development (e.g., proper naming and labeling of classes and properties).
You can also provide your own SHACL shapes in a directory named `shacl` at the root of your repository.

### Documentation

#### `Linking Diagrams`

OntoFlow generates the documentation with pyLODE, for the parts of your ontology which you created with a Chowlk diagram you can add those diagrams to your pyLODE documentation. The name of the exported picture is the same as your Chowlk diagram, but with the `png` suffix.

```turtle
@prefix ex: <https://example/ontology#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .

ex:Class skos:example "example.png" .
```

#### `Adding Comments`

```turtle
@prefix ex: <https://example/ontology#> .
@prefix dct:   <http://purl.org/dc/terms/> .

ex:Class dct:description "Comment" .
```

#### `Versioning`

You can link to an older version of your ontology by adding the `owl:priorVersion` predicate to your ontology.

```turtle
@prefix ex: <https://example/ontology#> .

ex: owl:priorVersion "https://example/ontology#example.ttl" .
```

## Roadmap

TODO: Link to issues

- [Support ontology versioning](https://gitlab.com/infai/ontoflow/-/issues/17)
- provide page for data validation
- Podman support

## Contributing

TODO:

- how testing works

### Images

- ps installed
- Program in path
